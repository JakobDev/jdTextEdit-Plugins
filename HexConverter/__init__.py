from PyQt5.QtWidgets import QMenu, QAction
from jdTextEdit.Functions import showMessageBox

def main(env):
    converterMenu = QMenu("Converter",env.mainWindow)

    toHex = QAction("ASCII -> HEX",env.mainWindow)
    toHex.triggered.connect(lambda: convertHex(env))
    toHex.setData(["convertToHEX"])
    converterMenu.addAction(toHex)

    asciiAction = QAction("HEX -> ASCII",env.mainWindow)
    asciiAction.triggered.connect(lambda: convertAscii(env))
    asciiAction.setData(["convertToASCII"])
    converterMenu.addAction(asciiAction)

    env.mainWindow.toolsMenu.addMenu(converterMenu)

def convertHex(env):
    textWidget = env.mainWindow.getTextEditWidget()
    b = bytearray(textWidget.selectedText(),textWidget.getUsedEncoding())
    textWidget.replaceSelectedText(b.hex())

def convertAscii(env):
    textWidget = env.mainWindow.getTextEditWidget()
    try:
        textWidget.replaceSelectedText(bytearray.fromhex(textWidget.selectedText()).decode())
    except:
        showMessageBox("Error","Invalid hex format")

def getID():
    return "HexConverter"

def getName():
    return "Hex Converter"

def getVersion():
    return "1.0"

def getAuthor():
    return "JakobDev"
