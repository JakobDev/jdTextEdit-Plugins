from PyQt5.QtWidgets import QAction, QMenu
from jdTextEdit.Functions import showMessageBox
from xml.dom import minidom
import json

def main(env):
    beautifyMenu = QMenu("Beautify",env.mainWindow)

    xmlAction = QAction("Beautify XML",env.mainWindow)
    xmlAction.triggered.connect(lambda: beautifyXML(env))
    xmlAction.setData(["beautifyXML"])
    beautifyMenu.addAction(xmlAction)

    jsonAction = QAction("Beautify JSON",env.mainWindow)
    jsonAction.triggered.connect(lambda: beautifyJSON(env))
    jsonAction.setData(["beautifyJSON"])
    beautifyMenu.addAction(jsonAction)

    env.mainWindow.toolsMenu.addMenu(beautifyMenu)

def beautifyXML(env):
    try:
        editWidget = env.mainWindow.getTextEditWidget()
        xmlContent = minidom.parseString(editWidget.text())
        editWidget.setText(xmlContent.toprettyxml())
    except Exception as e:
        showMessageBox("Error",str(e))

def beautifyJSON(env):
    try:
        editWidget = env.mainWindow.getTextEditWidget()
        data = json.loads(editWidget.text())
        newString = json.dumps(data,indent=4)
        editWidget.setText(newString)
    except Exception as e:
        showMessageBox("Error",str(e))

def getID():
    return "SimpleBeautifyer"

def getName():
    return "SimpleBeautifyer"

def getVersion():
    return "1.0"

def getAuthor():
    return "JakobDev"
